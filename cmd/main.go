package main

import (
	"html/template"
	"io"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Templates struct {
	templates *template.Template
}

func (t *Templates) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func newTemplate() *Templates {
	return &Templates{
		templates: template.Must(template.ParseGlob("views/*.html")),
	}
}

type ToDo struct {
	Created     string
	Name        string
	Progress    string
	ShowOnBoard bool
	Id          int
}

var todoId = 0

func newToDo(name string, showOnBoard bool) ToDo {
	todoId++
	return ToDo{
		Created:     "0",
		Name:        name,
		Progress:    "TO_DO",
		ShowOnBoard: showOnBoard,
		Id:          todoId,
	}
}

type ToDos = []ToDo

type Data struct {
	ToDos ToDos
}

func (d *Data) indexOf(id int) int {
	for i, todo := range d.ToDos {
		if todo.Id == id {
			return i
		}
	}

	return -1
}

func newMockData() Data {
	return Data{
		ToDos: []ToDo{
			newToDo("learn htmx", true),
			newToDo("learn go", false),
		},
	}
}

type ToDoFormData struct {
	Errors      map[string]string
	Name        string
	ShowOnBoard bool
}

func newToDoFormData() ToDoFormData {
	return ToDoFormData{
		Name:        "",
		ShowOnBoard: false,
		Errors:      make(map[string]string),
	}
}

type Page struct {
	ToDoFormData ToDoFormData
	Data         Data
}

func newPage() Page {
	return Page{
		ToDoFormData: newToDoFormData(),
		Data:         newMockData(),
	}
}

func main() {
	e := echo.New()
	e.Use(middleware.Logger())

	e.Renderer = newTemplate()
	e.Static("/images", "images")
	e.Static("/css", "css")

	page := newPage()

	e.GET("/", func(c echo.Context) error {
		return c.Render(200, "index", page)
	})

	e.POST("/todos", func(c echo.Context) error {
		name := c.FormValue("name")
		show := c.FormValue("show")

		var showOnBoard bool
		if show == "show" {
			showOnBoard = true
		} else {
			showOnBoard = false
		}

		todo := newToDo(name, showOnBoard)
		page.Data.ToDos = append(page.Data.ToDos, todo)
		c.Render(200, "oob-todo", todo)
		return c.Render(200, "addTodoForm", newToDoFormData())
	})

	e.PATCH("/todos/:id", func(c echo.Context) error {
		idStr := c.Param("id")
		id, err := strconv.Atoi(idStr)
		if err != nil {
			return c.String(400, "Not a valid ID")
		}

		showParam := c.FormValue("show")
		show, err := strconv.ParseBool(showParam)
		if err != nil {
			return c.String(400, "Not a bool")
		}

		index := page.Data.indexOf(id)
		if index == -1 {
			return c.String(404, "Todo not found")
		}

		page.Data.ToDos[index].ShowOnBoard = show

		// page.Data.ToDos = append(page.Data.ToDos[:index], updatedTodo, page.Data.ToDos[index+1:]...)

		return c.Render(200, "todo", page.Data.ToDos[index])
	})

	e.DELETE("/todos/:id", func(c echo.Context) error {
		idStr := c.Param("id")
		id, err := strconv.Atoi(idStr)
		if err != nil {
			return c.String(400, "Not a valid ID")
		}

		index := page.Data.indexOf(id)
		if index == -1 {
			return c.String(404, "Todo not found")
		}

		time.Sleep(1 * time.Second)
		page.Data.ToDos = append(page.Data.ToDos[:index], page.Data.ToDos[index+1:]...)
		return c.NoContent(200)
	})

	e.Logger.Fatal(e.Start(":42420"))
}
